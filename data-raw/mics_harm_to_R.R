# Clean up the MICS Core Variables to harmonize.xlsx file for later import back into R for variable harmonization

library("data.table")
library("rio")
import::from(janitor, remove_empty)

# import spreadsheet from the current directory
# If needed, change the working directory to the location of the file
mics_crosswalk <- import("MICS Core Variables to harmonize.xlsx", sheet = "Cross Walk", skip = 3, setclass = "data.table")

# remove empty rows and columns
mics_crosswalk <- remove_empty(mics_crosswalk)


# remove single quotation marks/apostrophes from Variable label column
column_chose <- "Variable label"

# remove , (, ), and ' from Country names
for (col in column_chose) {
idx2 <- which(mics_crosswalk$"Variable label" %like% "`|'|‘|’")
set(mics_crosswalk, i = idx2, j = col, value = mgsub(mics_crosswalk[[col]][idx2], pattern = "`|'|‘|’", replacement = ""))
}

# save mics_crosswalk
save(mics_crosswalk, file = "MICS_Variables_Harmonize.RData")
